//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

var bodyparser = require('body-parser');
app.use(bodyparser.json())

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);
var movimientoJSON = require('./movimientosv2.json');


app.get ('/', function(req, res) {
  //res.send('Hola Mundo nodejs');
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.get ('/Clientes', function(req, res) {
  res.send('Aqui estan los clientes devueltos nuevos');
});

app.get ('/Clientes/:idcliente', function(req, res) {
  res.send('Aqui tiene al cliente numero:'+ req.params.idcliente);
});

app.get ('/Movimientos/v1', function(req, res) {
  res.sendfile('movimientosv1.json');
});

app.get ('/Movimientos/v2', function(req, res) {
  res.send(movimientoJSON);
});

app.get('/Movimientos/v2/:index/:otroParam', function(req, res) {
 console.log(req.params.index-1);
 console.log(req.params.otroParam);
 //res.send('Hola Mundo nodejs');
 res.send(movimientoJSON[req.params.index-1]);
});

app.get('/MovimientosQ/v2', function(req, res) {
 console.log(req.query);
 //res.send('Hola Mundo nodejs');
 res.send('Recibido');
});

app.post('/', function(req,res){
  res.send('Hemos recibido una peticion POST');
});

app.post('/Movimientos/v2', function(req,res){
  var nuevo = req.body
  nuevo.id = movimientoJSON.length + 1;
  movimientoJSON.push(nuevo)
  res.send('Movimiento dado de alta');
});
