#Imagen base
FROM node:latest

#directorio de la aplicacion en el contenedor
WORKDIR /app

#copiado de archivos
ADD . /app

#Dependencias
RUN npm install

#puerto que expongo
EXPOSE 3000

#Comando
CMD ["npm", "start"]
